/*
 *  operations.h
 *
 *  Jonatas Lopes de Paiva
 *  Marcos Sandim
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>

using namespace std;

namespace stringUtils
{
	/**
	 * Funcao auxiliar para eliminar espaços excedentes no início e fim de uma string
	 */
    string trim(string input)
    {
        if (input.length() == 0)
            return input;

        int i = 0;

        while(i < input.length() && isspace(input[i])) {
            i++;
        }

        if (i == input.length()) {
            return string();
        }

        int j = input.length() - 1;

        while(isspace(input[j]))
            j--;

        return input.substr(i, j - i + 1);

    }

	/**
	 * Funcao auxiliar para separar uma string em tokens
	 */
    vector<string> tokenizer(string input)
    {
        string clearStr = trim(input);
        vector<string> tokens;

        int i = 0;

        string token;

        while(i < clearStr.length())
        {
            while(i < clearStr.length() && !isspace(clearStr[i]))
                token += clearStr[i++];

            tokens.push_back(string(token));
            token.clear();

            while(i < clearStr.length() && isspace(clearStr[i]))
                i++;
        }

        return tokens;

    }
}

namespace fileOperations 
{
	/**
	 * Le o cabecalho de um arquivo de entrada do tipo PPM ou PGM. 
	 * A leitura se da pelo conteudo do arquivo e nao pela extensao do mesmo.
	 */
    int readHeader(ifstream &in, long int &width, long int &height, int &mode, int &maxValue)
    {
     string line;
     int part = 1;

     while(1)
     {

        if(!in.good())
                return 0; // error

            getline(in, line);
            line = stringUtils::trim(line);

            if(line.size() == 0 || line[0] == '#')
                continue;

            if(part == 1)
            {
                if(line.length() != 2)
                    return 0; // error

                if(line[0] == 'P')
                {
                    if(line[1] == '5')
                        mode = 1; // Grayscale
                    else if (line[1] == '6')
                        mode = 3; // RGB
                    else
                        return 0; // error
                    part = 2;
                }
            }
            else if (part == 2)
            {
                vector<string> tokens = stringUtils::tokenizer(line);
                if(tokens.size() != 2)
                    return 0; // error
                width = atol(tokens[0].c_str());
                height = atol(tokens[1].c_str());
                part = 3;
            }
            else if(part == 3)
            {
                maxValue = atoi(line.c_str());
                break;
            }

        }

        return 1;

    }

	/**
	 * Le o conteudo de um arquivo PPM ou PGM.
	 */
    unsigned char* readImage(ifstream &in, long int width, long int height, int mode)
    {
        unsigned char* output = new unsigned char[height*width*mode];

        unsigned char x;

        for(int i = 0; i < height; i++) {
            for(int j = 0; j < width; j++) {
                for(int k = 0; k < mode; k++) {
                    in.read((char*)&x, sizeof(x));
                    output[i*width*mode + j*mode + k] = x;        
                }            
            }
        }

        return output;

    }

	/**
	 * Escreve um arquivo PPM ou PGM como saida. O tipo do arquivo se da pelo conteudo e nao pela extensao.
	 */
    void writeOutput(ofstream &out, long int width, long int height, int mode, int maxValue, unsigned char* data)
    {
        if(mode == 1)
            out << "P5";
        else if (mode == 3)
            out << "P6";
        out << "\n# Output distributed computation.\n";
        out << width << " " << height << "\n";
        out << maxValue << "\n";

        unsigned char x;

        for(int i = 0; i < height; i++) {
            for(int j = 0; j < width; j++) {
                for(int k = 0; k < mode; k++) {
                    out.write((char*)&data[i*width*mode + j*mode + k], sizeof(unsigned char));
                }            
            }
        }
    }
}
