/*
 *  Trabalho2.cpp
 *
 *  Jonatas Lopes de Paiva
 *  Marcos Sandim
 */

#include <omp.h>
#include <mpi.h>
#include <iostream>
#include <time.h>
#include <sstream>
#include "operations.h"

/**
 * Realiza a filtragem em si
 */
unsigned char* avgFilterRGB(unsigned char* input, long startH, long endH, int mode, long width, long heightLimit) {

    unsigned char* output = new unsigned char[(endH-startH+1)*width*mode];
	
    double sum[mode];
	long i, j, k, m, z;
    #pragma omp parallel for schedule(static)
    for (long i = startH; i <= endH; i++) {

    for (long j = 0; j < width; j++) {

             for(int z = 0; z < mode; z++) {
                sum[z] = 0.0;
            }

            for (long k = i - 2; k <= i + 2; k++) {
                for (long m = j - 2; m <= j + 2; m++) {
                    if (k >= 0 && k < heightLimit && m >= 0 && m < width) {
                        for(int z = 0; z < mode; z++) {
                            sum[z] += input[k*width*mode + m*mode + z];
                        }
                    }
                }
            }

            for(int z = 0; z < mode; z++) {
                output[(i-startH)*width*mode + j*mode + z] = (unsigned char) (sum[z] / 25.0);
            }
        }
    }

    return output;
}

/**
 * Struct para envio de informacoes via MPI
 */
struct StripInfo {
    int overlapTop;
    int overlapBottom;
    long height;
    long width;
    int channels;
};

int main(int argc, char** argv) {
    /*
     * Inicia uma sessão MPI
     */
    MPI_Init(&argc, &argv);

    int rank, numProcesses, rc;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);

    int infoTag = 1;
    int dataTag = 2;
    int responseTag = 3;

    MPI_Status Stat;

	// Processo mestre
    if (rank == 0) {

		// Comeca a calcular o tempo de execucao do algoritmo
        struct timespec start, finish;
        double elapsed;

        clock_gettime(CLOCK_MONOTONIC, &start);

        std::ifstream input(argv[1], ios::binary);

        if(! input.good())
        {
            std::cout <<  "Could not open or find the image\n";
            return -1;
        }

        long int width, height;
        int mode, maxValue;

        int fileGood = fileOperations::readHeader(input, width, height, mode, maxValue);
		
		if(! fileGood)
        {
            std::cout <<  "File not properly formatted\n";
            return -1;
        }

		// Leitura do conteudo da imagem
        unsigned char* image = fileOperations::readImage(input, width, height, mode);

        input.close();

		// Calcula a fatia pela qual cada processo sera responsavel e seta variaveis utilizadas por todos
        int numWorkers = numProcesses - 1;
        int overlapSize = 2;
        int overlapTop = 0;
        int overlapBottom = overlapSize;
        int stripHeight = int(height/numProcesses) + 1;
        int lastStripHeight = height - (stripHeight*numWorkers);
        int stripSkip = stripHeight - overlapSize;
        int stripWidth = width;
        long totalSize;
        StripInfo info;
        info.width = width;
        info.channels = mode;
        info.overlapBottom = overlapBottom;

        unsigned char* p = image;

		
		std::cout << "Altura total imagem: " << height << "\t Largura: " << width << "\n\n";
		
		// Envia a parte da imagem pelo qual cada escravo sera responsavel
        for (int process = 1; process <= numWorkers; ++process) {
            std::cout << "Processo: " << process << "\t";
            std::cout << "Sobreposicao superior: " << overlapTop << "\t Sobreposicao inferior: " << overlapBottom << "\t";
            std::cout << "Altura faixa: " << stripHeight + overlapTop + overlapBottom << "\t Largura da faixa: " << stripWidth << "\n";

            info.overlapTop = overlapTop;
            info.height = (stripHeight + overlapTop + overlapBottom);

            totalSize = info.width * info.height * info.channels;

            rc = MPI_Send(&info, sizeof(StripInfo), MPI_CHAR, process, infoTag, MPI_COMM_WORLD);

            rc = MPI_Send((void*)p, totalSize, MPI_CHAR, process, dataTag, MPI_COMM_WORLD);

            p += stripWidth * (stripSkip + overlapTop) * info.channels;

            overlapTop = overlapSize;
        }
        overlapBottom = 0;

		std::cout << "Processo: " << rank << "\t";
		std::cout << "Faixa superior: " << overlapTop << "\t Faixa inferior: " << overlapBottom << "\t";
		std::cout << "Altura faixa: " << lastStripHeight + overlapTop + overlapBottom << "\t Largura da faixa: " << stripWidth << "\n";

		// O processo mestre realiza o trabalho na ultima faixa
        p = avgFilterRGB(p, overlapTop, overlapTop+lastStripHeight-1, mode, width, overlapTop+lastStripHeight);

        overlapTop = 0;
        overlapBottom = overlapSize;

        unsigned char* result = image;

		// Recebe os resultados dos subprocessos
        for (int process = 1; process <= numWorkers; ++process) {

            totalSize = info.width * stripHeight * info.channels;

            unsigned char *imageData = new unsigned char[totalSize];

            rc = MPI_Recv((void*)imageData, totalSize, MPI_CHAR, process, responseTag, MPI_COMM_WORLD, &Stat);
			
			// Os valores recebidos vao sobrescrevendo a imagem original para nao alocar mais memoria que o necessario
            memcpy(result, imageData, totalSize);

            delete[] imageData;

            result += totalSize;

            overlapTop = overlapSize;
        }

        totalSize = info.width * lastStripHeight * info.channels;

        memcpy(result, p, totalSize);
        delete[] p;

        std::ofstream out(argv[2], ios::binary);

		// Escreve a saida
        fileOperations::writeOutput(out, width, height, mode, maxValue, image);

        out.close();

        delete[] image;

        clock_gettime(CLOCK_MONOTONIC, &finish);

        // Calcula o tempo de execucao do algoritmo e imprime na tela
        elapsed = (finish.tv_sec - start.tv_sec);
        elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

        std::cout << "Tempo de execucao do algoritmo: " << elapsed << "\n\n";

    }
    else 
	{
		// Responsabilidade de cada escravo
        StripInfo info;
        int totalSize;
        int source = 0;

		// Recebe os dados
        rc = MPI_Recv(&info, sizeof(StripInfo), MPI_CHAR, source, infoTag, MPI_COMM_WORLD, &Stat);

        totalSize = info.width * info.height * info.channels;
        unsigned char *imageData = new unsigned char[totalSize];
        rc = MPI_Recv(imageData, totalSize, MPI_CHAR, source, dataTag, MPI_COMM_WORLD, &Stat);

		// Calcula as medias
        unsigned char *p = avgFilterRGB(imageData, info.overlapTop, info.height - info.overlapBottom - 1, info.channels, 
            info.width, info.height);

        totalSize = info.width * (info.height - info.overlapBottom - info.overlapTop) * info.channels;

		// Retorna os dados pro processo mestre
        rc = MPI_Send((void*)p, totalSize, MPI_CHAR, source, responseTag, MPI_COMM_WORLD);

        delete[] imageData;
        delete[] p;

    }

    MPI_Finalize();
    return 0;
}

